package com.taahyt.skyblock.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Logger
{

    public void error(String message)
    {
        Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[SKYBLOCK ERROR] " + ChatColor.RESET + message);
    }

    public void error(String message, ChatColor color)
    {
        Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[SKYBLOCK ERROR] " + color + message);
    }

    public void info(String message)
    {
        Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[Skyblock] " + ChatColor.RESET + message);
    }

    public void info(String message, ChatColor color)
    {
        Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[Skyblock] " + color + message);
    }


}
