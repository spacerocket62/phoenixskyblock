package com.taahyt.skyblock.island;

import com.taahyt.skyblock.Skyblock;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class IslandManager
{

    private Skyblock plugin = Skyblock.getPlugin(Skyblock.class);

    public HashMap<UUID, Island> islands = new HashMap<UUID, Island>();

    public Island getIslandByWorld(World w)
    {
        for (Island island : islands.values())
        {
            if (island.getWorld().equals(w))
            {
                return island;
            }
        }
        return null;
    }

    public void saveData(Island island)
    {
        String uuidOwner = island.getOwner();
        List<String> members = island.getTeamMembers();
        String worldName = island.getWorld().getName();

        File f = new File(plugin.getDataFolder(), "islands.yml");
        YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);
        if (conf.getConfigurationSection("islands." + uuidOwner) == null)
        {
            conf.createSection("islands." + uuidOwner);
            conf.set("islands." + uuidOwner + ".world", worldName);
            conf.set("islands." + uuidOwner + ".ownerName", Bukkit.getOfflinePlayer(UUID.fromString(island.getOwner())).getName());
            conf.set("islands." + uuidOwner + ".members", members);
            try {
                conf.save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            conf.set("islands." + uuidOwner + ".ownerName", Bukkit.getOfflinePlayer(UUID.fromString(island.getOwner())).getName());
            conf.set("islands." + uuidOwner + ".members", members);
            try {
                conf.save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadIslandData()
    {
        File f = new File(plugin.getDataFolder(), "islands.yml");
        YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);
        for (String key : conf.getConfigurationSection("islands").getKeys(false))
        {
            String prefix = "islands." + key;
            World world = Bukkit.getWorld(conf.getString(prefix + ".world"));
            String owner = key;
            List<String> members = new ArrayList<String>();
            for (String s : conf.getStringList(prefix + ".members"))
            {
                members.add(s);
            }
            Island island = new Island(world, owner, members);
            islands.put(UUID.fromString(owner), island);
        }
    }

    public void saveAllIslands()
    {
        for (Island is : islands.values())
        {
            saveData(is);
        }
    }



}
