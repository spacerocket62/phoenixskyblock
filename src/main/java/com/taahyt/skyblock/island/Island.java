package com.taahyt.skyblock.island;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormats;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardReader;
import com.sk89q.worldedit.function.operation.Operation;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.session.ClipboardHolder;
import com.taahyt.skyblock.Skyblock;
import com.taahyt.skyblock.player.SkyblockPlayer;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Island
{

    private Skyblock plugin = Skyblock.getPlugin(Skyblock.class);

    private World world;
    private String owner;
    private List<String> teamMembers;

    public Island(World world, String ownerUUID, List<String> teamMembers)
    {
        this.world = world;
        this.owner = ownerUUID;
        this.teamMembers = teamMembers;
    }

    public void generateIsland(World world) {
        WorldEditPlugin wEplugin = (WorldEditPlugin) plugin.getServer().getPluginManager().getPlugin("WorldEdit");

        File file = new File(wEplugin.getDataFolder() + File.separator + "schematics", "default-island.schem");
        Vector to = new Vector(102, 99, 102);

        ClipboardFormat format = ClipboardFormats.findByFile(file);

        ClipboardReader reader = null;
        try {
            reader = format.getReader(new FileInputStream(file));
            Clipboard clipboard = reader.read();

            com.sk89q.worldedit.world.World adaptedWorld = BukkitAdapter.adapt(world);

            EditSession editSession = WorldEdit.getInstance().getEditSessionFactory().getEditSession(adaptedWorld, -1);

            Operation operation = new ClipboardHolder(clipboard).createPaste(editSession)
            .to(BlockVector3.at(102, 101, 102)).ignoreAirBlocks(true).build();


            try { // This simply completes our paste and then cleans up.
                Operations.complete(operation);
                editSession.flushSession();

            } catch (WorldEditException e) { // If worldedit generated an exception it will go here
                plugin.logger.info("OOPS! Something went wrong, please contact an administrator");
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public List<String> getOnlyMembers()
    {
        List<String> members = new ArrayList<String>();
        members.addAll(getTeamMembers());;
        members.remove(getOwner());
        return members;
    }

}
