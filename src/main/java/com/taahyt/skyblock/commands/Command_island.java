package com.taahyt.skyblock.commands;

import com.taahyt.skyblock.Skyblock;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_island implements CommandExecutor
{

    private Skyblock plugin = Skyblock.getPlugin(Skyblock.class);

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (!(sender instanceof Player))
        {
            plugin.logger.error(Messages.PLAYER_ONLY, ChatColor.RED);
            return true;
        }

        Player player = (Player) sender;

        plugin.ownerGUI.openInventory(player);

        return true;
    }

}
