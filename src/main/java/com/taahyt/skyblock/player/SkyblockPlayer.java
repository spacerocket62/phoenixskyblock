package com.taahyt.skyblock.player;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

@Getter
@Setter
public class SkyblockPlayer
{

    private UUID id;
    private String islandID;
    private int coins;

    public SkyblockPlayer(UUID id)
    {
        this.id = id;
    }

    public Player getPlayer()
    {
        Player player = Bukkit.getPlayer(getId());
        if (player != null)
        {
            return player;
        } else {
            return (Player) Bukkit.getOfflinePlayer(getId());
        }
    }

    public String getName()
    {
        return getPlayer().getName();
    }

    public String getIP()
    {
        return getPlayer().getAddress().getAddress().getHostAddress();
    }
}
