package com.taahyt.skyblock.player;

import com.taahyt.skyblock.Skyblock;
import com.taahyt.skyblock.island.Island;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SkyblockPlayerManager
{
    private Skyblock plugin = Skyblock.getPlugin(Skyblock.class);

    public Map<UUID, SkyblockPlayer> players = new HashMap<UUID, SkyblockPlayer>();

    public SkyblockPlayer getSkyblockPlayer(UUID id)
    {
        return players.get(id);
    }

    public void saveData(SkyblockPlayer player)
    {
        String uuid = player.getId().toString();
        String islandWorldID =  player.getIslandID();
        String name = player.getName();
        int coins = player.getCoins();

        File f = new File(plugin.getDataFolder(), "players.yml");
        YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);
        if (conf.getConfigurationSection("players." + uuid) == null)
        {
            conf.createSection("players." + uuid);
            conf.set("players." + uuid + ".name", name);
            conf.set("players." + uuid + ".islandWorldID", islandWorldID);
            conf.set("players." + uuid + ".coins", coins);
            try {
                conf.save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            conf.set("players." + uuid + ".name", name);
            conf.set("players." + uuid + ".islandWorldID", islandWorldID);
            conf.set("players." + uuid + ".coins", coins);
            try {
                conf.save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadFromData(Player player)
    {
        String uuid = player.getUniqueId().toString();
        File f = new File(plugin.getDataFolder(), "players.yml");
        YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);
        String islandID = conf.getString("players." + uuid + ".islandWorldID");
        int coins = conf.getInt("players." + uuid + ".coins");

        SkyblockPlayer skyPlayer = new SkyblockPlayer(player.getUniqueId());
        skyPlayer.setIslandID(islandID);
        skyPlayer.setCoins(coins);
        players.put(player.getUniqueId(), skyPlayer);
    }

    public boolean exists(Player player)
    {
        File f = new File(plugin.getDataFolder(), "players.yml");
        YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);
        String entry = conf.getString("players." + player.getUniqueId().toString());
        if (entry == null)
        {
            return false;
        }
        return true;
    }

    public SkyblockPlayer getFromName(String name)
    {
        for (SkyblockPlayer player : players.values())
        {
            if (player.getName().equalsIgnoreCase(name))
            {
                return player;
            }
        }
        return null;
    }

    public boolean playerOwnsIsland(SkyblockPlayer player)
    {
        for (Island is : plugin.islandManager.islands.values())
        {
            if (is.getOwner().equalsIgnoreCase(player.getId().toString()))
            {
                return true;
            }
        }
        return false;
    }

    public boolean playerIsMemberOfIsland(SkyblockPlayer player)
    {
        for (Island is : plugin.islandManager.islands.values())
        {
            if (!is.getOwner().equalsIgnoreCase(player.getId().toString()) && is.getTeamMembers().contains(player.getId().toString()))
            {
                return true;
            }
        }
        return false;
    }

    public void goHome(SkyblockPlayer player)
    {
        if (playerOwnsIsland(player))
        {
            player.getPlayer().teleport(Bukkit.getWorld(player.getId().toString().substring(0, 8)).getSpawnLocation());
            return;
        }
        else if (playerIsMemberOfIsland(player))
        {
            for (Island is : plugin.islandManager.islands.values())
            {
                if (is.getOnlyMembers().contains(player.getId().toString()))
                {
                    player.getPlayer().teleport(is.getWorld().getSpawnLocation());
                }
            }
        }
    }

}
