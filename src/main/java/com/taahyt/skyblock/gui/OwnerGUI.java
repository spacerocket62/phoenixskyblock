package com.taahyt.skyblock.gui;

import com.taahyt.skyblock.Skyblock;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class OwnerGUI
{
    private Skyblock plugin;
    public Inventory inventory;

    public OwnerGUI(Skyblock plugin)
    {
        this.plugin = plugin;
        this.inventory = Bukkit.createInventory(null, 54, "    §d§lIsland GUI: Owner");
    }

    public void openInventory(Player player)
    {
        ItemStack home = new ItemStack(Material.RED_BED);
        ItemMeta meta = home.getItemMeta();
        assert meta != null;
        meta.setDisplayName(ChatColor.RED + "Home");
        home.setItemMeta(meta);


        inventory.setItem(22, home);

        for (int i = 0; i < inventory.getSize(); i++) {
            if(inventory.getItem(i) == null || inventory.getItem(i).getType().equals(Material.AIR)) {
                inventory.setItem(i, new ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1, (short) plugin.getConfig().getInt("lobbyselector.stained-glass-dv")));
            }
        }

        player.openInventory(inventory);

    }

}
