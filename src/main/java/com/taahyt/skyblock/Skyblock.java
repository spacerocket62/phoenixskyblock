package com.taahyt.skyblock;

import com.taahyt.skyblock.commands.CommandHandler;
import com.taahyt.skyblock.gui.OwnerGUI;
import com.taahyt.skyblock.island.IslandManager;
import com.taahyt.skyblock.listeners.ListenerHandler;
import com.taahyt.skyblock.player.SkyblockPlayerManager;
import com.taahyt.skyblock.util.Logger;
import com.taahyt.skyblock.world.SlimeWorldManagerPlugin;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class Skyblock extends JavaPlugin
{

    public Logger logger;

    public IslandManager islandManager;
    public SkyblockPlayerManager playerManager;
    public SlimeWorldManagerPlugin slimeManager;

    public ListenerHandler listenerHandler;
    public CommandHandler commandHandler;

    public File islands;
    public File players;

    public OwnerGUI ownerGUI;

    @Override
    public void onLoad()
    {
        this.logger = new Logger();

        this.islandManager = new IslandManager();
        this.playerManager = new SkyblockPlayerManager();

        this.listenerHandler = new ListenerHandler();
        this.commandHandler = new CommandHandler();

        this.slimeManager = new SlimeWorldManagerPlugin(this);


        logger.info("Loading Phoenix Skyblock", ChatColor.GOLD);
    }

    @Override
    public void onEnable()
    {
        this.ownerGUI = new OwnerGUI(this);

        if (!getDataFolder().exists())
        {
            getDataFolder().mkdir();
        }
        players = new File(getDataFolder(), "players.yml");
        islands = new File(getDataFolder(), "islands.yml");
        if (!islands.exists())
        {
            try {
                islands.createNewFile();
                YamlConfiguration conf = YamlConfiguration.loadConfiguration(islands);
                conf.createSection("islands");
                conf.save(islands);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!players.exists())
        {
            try {
                players.createNewFile();
                YamlConfiguration conf = YamlConfiguration.loadConfiguration(players);
                conf.createSection("players");
                conf.save(players);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        this.listenerHandler.register(this);
        this.commandHandler.register(this);

        this.slimeManager.loadWorlds();

        logger.info("Enabling Phoenix Skyblock", ChatColor.GOLD);


        logger.info("These are the following worlds: " + StringUtils.join(slimeManager.getAllWorlds(), ", "));

        logger.info("Worlds List: " + StringUtils.join(getServer().getWorlds(), ", "));

        islandManager.loadIslandData();
    }


    @Override
    public void onDisable()
    {
        logger.info("Disabling Phoenix Skyblock", ChatColor.GOLD);
        slimeManager.saveAllWorlds();
        islandManager.saveAllIslands();
    }

}
