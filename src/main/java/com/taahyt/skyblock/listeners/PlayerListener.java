package com.taahyt.skyblock.listeners;

import com.taahyt.skyblock.Skyblock;
import com.taahyt.skyblock.island.Island;
import com.taahyt.skyblock.player.SkyblockPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Arrays;

public class PlayerListener implements Listener
{
    private Skyblock plugin = Skyblock.getPlugin(Skyblock.class);

    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();

        if (!plugin.playerManager.exists(player))
        {
            SkyblockPlayer skyPlayer = new SkyblockPlayer(player.getUniqueId());
            skyPlayer.setIslandID(player.getUniqueId().toString().substring(0, 8));
            skyPlayer.setCoins(0);
            plugin.logger.info(skyPlayer.getName(), ChatColor.GREEN);
            plugin.logger.info(skyPlayer.getIslandID(), ChatColor.AQUA);
            plugin.logger.info(skyPlayer.getIP(), ChatColor.AQUA);
            plugin.logger.info(skyPlayer.getId().toString(), ChatColor.AQUA);
            plugin.logger.info(String.valueOf(skyPlayer.getCoins()), ChatColor.AQUA);
            plugin.slimeManager.createWorld(player.getUniqueId().toString().substring(0, 8), 0, false, false, 106,94, 99, false, false);

            World w = Bukkit.getWorld(player.getUniqueId().toString().substring(0, 8));

            player.teleport(Bukkit.getWorld(player.getUniqueId().toString().substring(0, 8)).getSpawnLocation());

            Island island = new Island(w, player.getUniqueId().toString(), Arrays.asList(player.getUniqueId().toString()));
            island.generateIsland(Bukkit.getWorld(player.getUniqueId().toString().substring(0, 8)));

            plugin.playerManager.players.put(player.getUniqueId(), skyPlayer);
            plugin.islandManager.islands.put(player.getUniqueId(), island);
        } else {
            plugin.playerManager.loadFromData(player);
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event)
    {
        Player player = event.getPlayer();
        plugin.playerManager.saveData(plugin.playerManager.getSkyblockPlayer(player.getUniqueId()));
    }

    @EventHandler
    public void onSwitch(PlayerChangedWorldEvent event)
    {
        Player player = event.getPlayer();
        World to = player.getWorld();

        if (!to.getWorldFolder().getName().endsWith(".slime"))
        {
            return;
        }

        Island island = plugin.islandManager.getIslandByWorld(to);
        if (island.getTeamMembers().contains(player.getUniqueId().toString()))
        {
            player.setAllowFlight(true);
        } else {
            player.setAllowFlight(false);
        }

    }

}