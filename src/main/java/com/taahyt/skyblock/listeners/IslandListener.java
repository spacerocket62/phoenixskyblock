package com.taahyt.skyblock.listeners;

import com.taahyt.skyblock.Skyblock;
import com.taahyt.skyblock.island.Island;
import com.taahyt.skyblock.player.SkyblockPlayer;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.List;

public class IslandListener implements Listener
{

    private Skyblock plugin = Skyblock.getPlugin(Skyblock.class);


    @EventHandler
    public void onBreak(BlockBreakEvent event)
    {
        Player player = event.getPlayer();
        World w = event.getPlayer().getWorld();
        Island island = plugin.islandManager.getIslandByWorld(w);
        List<String> members = island.getTeamMembers();


        if (!members.contains(player.getUniqueId().toString()))
        {
            player.sendMessage(ChatColor.RED + "You are not a member of this island.");
            event.setCancelled(true);
            return;
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event)
    {
        Player player = event.getPlayer();
        World w = event.getPlayer().getWorld();
        Island island = plugin.islandManager.getIslandByWorld(w);
        List<String> members = island.getTeamMembers();
        if (!members.contains(player.getUniqueId().toString()))
        {
            player.getPlayer().sendMessage(ChatColor.RED + "You are not a member of this island.");
            event.setCancelled(true);
            return;
        }
    }


}
