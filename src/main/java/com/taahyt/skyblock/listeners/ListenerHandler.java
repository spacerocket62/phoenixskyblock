package com.taahyt.skyblock.listeners;

import com.taahyt.skyblock.Skyblock;
import com.taahyt.skyblock.listeners.gui.OwnerGUIListener;

public class ListenerHandler
{

    public void register(Skyblock plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(new PlayerListener(), plugin);
        plugin.getServer().getPluginManager().registerEvents(new IslandListener(), plugin);
        plugin.getServer().getPluginManager().registerEvents(new OwnerGUIListener(), plugin);
    }

}
