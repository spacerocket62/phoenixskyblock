package com.taahyt.skyblock.listeners.gui;

import com.taahyt.skyblock.Skyblock;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

public class OwnerGUIListener implements Listener
{

    private Skyblock plugin = Skyblock.getPlugin(Skyblock.class);

    @EventHandler
    public void onClick(InventoryClickEvent event)
    {
        InventoryView view = event.getView();
        final Player player = (Player) event.getWhoClicked();
        ItemStack item = event.getCurrentItem();

        if (view.getTitle() != "    §d§lIsland GUI: Owner")
        {
            return;
        }

        assert item != null;
        if (item.hasItemMeta())
        {
            if (item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.RED + "Home"))
            {

                event.setCancelled(true);
                player.closeInventory();

                final BukkitTask task = new BukkitRunnable() {
                    @Override
                    public void run() {
                        player.getWorld().playEffect(player.getLocation(), Effect.EXTINGUISH, 60);
                        player.playSound(player.getLocation(), Sound.BLOCK_SMOKER_SMOKE, 100, 100);
                    }
                }.runTaskTimer(plugin, 1, 40);

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        task.cancel();
                        player.setVelocity(new Vector(0, 5, 0));
                    }
                }.runTaskLater(plugin, 60);

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        plugin.playerManager.goHome(plugin.playerManager.getSkyblockPlayer(player.getUniqueId()));
                    }
                }.runTaskLater(plugin, 100);

                return;
            }
        } else {
            event.setCancelled(true);
            return;
        }
    }

}
