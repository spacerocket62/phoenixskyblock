package com.taahyt.skyblock.world;

import com.grinderwolf.swm.api.SlimePlugin;
import com.grinderwolf.swm.api.exceptions.*;
import com.grinderwolf.swm.api.loaders.SlimeLoader;
import com.grinderwolf.swm.api.world.SlimeWorld;
import com.grinderwolf.swm.api.world.properties.SlimeProperties;
import com.taahyt.skyblock.Skyblock;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.List;

public class SlimeWorldManagerPlugin
{

    public SlimePlugin slime;
    public Skyblock plugin;

    public SlimeWorldManagerPlugin(Skyblock plugin)
    {
        this.plugin = plugin;
        try {
            this.slime = (SlimePlugin) Bukkit.getPluginManager().getPlugin("SlimeWorldManager");
            plugin.logger.info("Successfully enabled SlimeWorldManager", ChatColor.GREEN);
        } catch (Exception e)
        {
            e.printStackTrace();
            plugin.logger.error("Unable to find 'SlimeWorldManager' plugin! Most features will not work!", ChatColor.RED);
        }
    }

    public SlimeLoader getLoader()
    {
        return this.slime.getLoader("file");
    }

    public void createWorld(String name, int difficulty, boolean animals, boolean monsters, int spawnX, int spawnY, int spawnZ, boolean pvp, boolean readOnly)
    {
        SlimeWorld.SlimeProperties props = SlimeWorld.SlimeProperties.builder()
                .difficulty(difficulty)
                .allowAnimals(animals)
                .allowMonsters(monsters)
                .spawnX(spawnX)
                .spawnY(spawnY)
                .spawnZ(spawnZ)
                .pvp(pvp)
                .readOnly(readOnly)
                .build();
        try {
            if (!getLoader().worldExists(name))
            {
                SlimeWorld world = null;
                try {
                    world = slime.createEmptyWorld(getLoader(), name, props);
                    slime.generateWorld(world);
                } catch (WorldAlreadyExistsException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> getAllWorlds()
    {
        try {
            return getLoader().listWorlds();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void saveAllWorlds()
    {
        File f = new File(plugin.getServer().getWorldContainer() + File.separator + "slime_worlds");
        for (World w : plugin.getServer().getWorlds())
        {
            w.save();
        }
        for (String s : getAllWorlds())
        {
            try {
                File file = new File(f, s + ".slime");
                InputStream slimeWorld = new FileInputStream(file);
                byte[] world = IOUtils.toByteArray(slimeWorld);
                getLoader().saveWorld(s, world, false);
                plugin.logger.info("Successfully saved " + getAllWorlds().size() + " worlds.", ChatColor.GREEN);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadWorlds()
    {
        for (String s : getAllWorlds())
        {
            try {
                SlimeWorld.SlimeProperties props = SlimeWorld.SlimeProperties.builder()
                    .difficulty(0)
                    .allowAnimals(false)
                    .allowMonsters(false)
                    .spawnX(106)
                    .spawnY(94)
                    .spawnZ(99)
                    .pvp(false)
                    .readOnly(false)
                    .build();
                SlimeWorld world = slime.loadWorld(getLoader(), s, props);
                slime.generateWorld(world);
            } catch (UnknownWorldException e) {
                e.printStackTrace();
            } catch (WorldInUseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NewerFormatException e) {
                e.printStackTrace();
            } catch (CorruptedWorldException e) {
                e.printStackTrace();
            }
        }
    }

}
